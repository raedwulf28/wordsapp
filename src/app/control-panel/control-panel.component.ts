import { ChangeDetectionStrategy, Component, OnInit, ChangeDetectorRef } from '@angular/core';
import { TextService } from '../text-service/text.service';
import { actions } from '../model/actions-enum';

@Component({
  selector: 'app-control-panel',
  templateUrl: './control-panel.component.html',
  styleUrls: ['./control-panel.component.scss'],
  changeDetection: ChangeDetectionStrategy.OnPush
})
export class ControlPanelComponent implements OnInit {
  textSelected = false;
  constructor(
    private textService: TextService,
    private cdr: ChangeDetectorRef
  ) { }

  ngOnInit(): void {
    this.textService.istextSelected$.subscribe(value => {
      this.textSelected = value;
      this.cdr.detectChanges();
    });
  }

  boldAction(): void {
    this.textService.actionExec(actions.bold);
  }

  italicAction(): void {
    this.textService.actionExec(actions.italic);
  }

  underlineAction(): void {
    this.textService.actionExec(actions.underline);
  }

}
