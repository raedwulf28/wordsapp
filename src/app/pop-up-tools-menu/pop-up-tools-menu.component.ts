import { Component, OnInit, Inject } from '@angular/core';
import { MatDialogRef, MAT_DIALOG_DATA } from '@angular/material/dialog';
import { TextService } from '../text-service/text.service';

@Component({
  selector: 'app-pop-up-tools-menu',
  templateUrl: './pop-up-tools-menu.component.html',
  styleUrls: ['./pop-up-tools-menu.component.scss']
})
export class PopUpToolsMenuComponent implements OnInit {
  synonims: ApiResults[] = [];
  results = '';
  indexWord: number;
  constructor(
    public dialogRef: MatDialogRef<PopUpToolsMenuComponent>,
    private textService: TextService,
    @Inject(MAT_DIALOG_DATA) public data: { word: string }) {}

  onNoClick(): void {
    this.dialogRef.close();
  }
  ngOnInit() {
    this.textService.getSynonims(this.data.word).subscribe(result => {
      this.synonims = result as ApiResults[];
      console.log(this.synonims);
    });
  }

  synonimSelected(word: string, index: number): void {
    this.results = word;
    this.indexWord = index;
  }

}

interface ApiResults {
  word: string;
  tags: string[];
  score: number;
}
