import { BrowserModule } from '@angular/platform-browser';
import { NgModule } from '@angular/core';
import { BrowserAnimationsModule } from '@angular/platform-browser/animations';
import { HttpClientModule } from '@angular/common/http';
import { AppComponent } from './app.component';
import { FileComponent } from './file/file.component';
import { ControlPanelComponent } from './control-panel/control-panel.component';
import { HeaderComponent } from './header/header.component';
import { FormsModule } from '@angular/forms';
import { TextService } from './text-service/text.service';
import { FooterComponent } from './footer/footer.component';
import { AppWordsComponent } from './app-words/app-words.component';
import { PopUpToolsMenuComponent } from './pop-up-tools-menu/pop-up-tools-menu.component';
import { MatDialogModule } from '@angular/material/dialog';

@NgModule({
   declarations: [
      AppComponent,
      FileComponent,
      ControlPanelComponent,
      HeaderComponent,
      FooterComponent,
      AppWordsComponent,
      PopUpToolsMenuComponent
   ],
   imports: [
      BrowserModule,
      FormsModule,
      BrowserAnimationsModule,
      HttpClientModule,
      MatDialogModule
   ],
   providers: [
      TextService
   ],
   entryComponents: [
      PopUpToolsMenuComponent
   ],
   bootstrap: [
      AppComponent
   ]
})
export class AppModule {
}
