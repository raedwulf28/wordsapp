export enum actions {
  none = 0,
  bold,
  italic,
  underline
}
