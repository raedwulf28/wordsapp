import {
  Component,
  OnInit,
  Input,
  OnChanges,
  SimpleChanges,
  ChangeDetectionStrategy,
  ChangeDetectorRef,
  Output,
  EventEmitter
} from '@angular/core';
import { TextService } from '../text-service/text.service';
import { actions } from '../model/actions-enum';
import { PopUpToolsMenuComponent } from '../pop-up-tools-menu/pop-up-tools-menu.component';
import { MatDialog } from '@angular/material/dialog';

@Component({
  selector: 'app-words',
  templateUrl: './app-words.component.html',
  styleUrls: ['./app-words.component.scss'],
  changeDetection: ChangeDetectionStrategy.OnPush
})
export class AppWordsComponent implements OnInit, OnChanges {
  @Input() word: '';
  @Output() selectAction = new EventEmitter<boolean>(false);
  isSelected = false;
  styles: actions[] = [];
  isItalic = false;
  isBold = false;
  isUnderline = false;
  constructor(
    private textService: TextService,
    private cdr: ChangeDetectorRef,
    public dialog: MatDialog) { }

  ngOnInit(): void {
    this.getActions();
  }

  ngOnChanges(changes: SimpleChanges): void {

  }

  onSelectWord(): void {
    this.isSelected = !this.isSelected;
    if (!!this.isSelected) {
      this.textService.textSelection(true);
      this.selectAction.emit(true);
      this.cdr.detectChanges();
    } else {
      this.selectAction.emit(false);
    }
  }

  getActions(): void {
    this.textService.action$
      .subscribe(value => {
        if (!!this.isSelected) {
          const indexStyleApplied = this.styles.findIndex(style => style === value);
          if (indexStyleApplied > -1) {
            this.styles.splice(indexStyleApplied, 1);
          } else {
            this.styles.push(value);
          }
          this.checkStylesApplied();
        }

      });
  }

  checkStylesApplied(): void {
    this.cleanStyles();
    for (const style of this.styles) {
      switch (style) {
        case actions.bold:
          this.isBold = true;
          break;
        case actions.italic:
          this.isItalic = true;
          break;
        case actions.underline:
          this.isUnderline = true;
          break;
        default:
      }
    }
    this.cdr.detectChanges();
  }

  cleanStyles(): void {
    this.isBold = false;
    this.isItalic = false;
    this.isUnderline = false;
  }

  onDoubleClickWord(): void {
    const dialogRef = this.dialog.open(PopUpToolsMenuComponent, {
      width: '300px',
      data: { word: this.word }
    });

    dialogRef.afterClosed().subscribe(result => {
      if (!!result) {
        this.word = result;
        this.cdr.detectChanges();
      }
    });
  }
}
