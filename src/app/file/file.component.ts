import { ChangeDetectionStrategy, Component, OnInit, ChangeDetectorRef, ViewChildren, QueryList } from '@angular/core';
import { TextService } from '../text-service/text.service';
import { AppWordsComponent } from '../app-words/app-words.component';

@Component({
  selector: 'app-file',
  templateUrl: './file.component.html',
  styleUrls: ['./file.component.scss'],
  changeDetection: ChangeDetectionStrategy.OnPush
})
export class FileComponent implements OnInit {
  @ViewChildren(AppWordsComponent) appWords: QueryList<AppWordsComponent>;
  text$: Promise<string>;
  words: string[] = [];
  constructor(
    private textService: TextService,
    private cdr: ChangeDetectorRef) {
  }

  ngOnInit() {
    this.text$ = this.textService.getMockText();
    this.getText();

  }

  getText(): void {
    this.text$.then(text => {
      this.splitText(text);
    });
  }

  splitText(text: string): void {
    this.words = text.split(' ');
    this.cdr.detectChanges();
  }

  onSelectionAction(event: boolean): void {
    const someSelected = this.appWords.some(appWord => {
      return appWord.isSelected === true;
    });
    if (!someSelected) {
      this.textService.textSelection(false);
    }
  }
}
