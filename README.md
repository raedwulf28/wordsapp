# AeFrontendTexteditorAngularSkeleton

This project was generated with [Angular CLI](https://github.com/angular/angular-cli) version 7.2.3.

## Development server

Run `ng serve` for a dev server. Navigate to `http://localhost:4200/`. The app will automatically reload if you change any of the source files.

## Using

- To select a word to be changed just click the word.
- To the words can add bold, underline and italic styles.
- To see the Synonims, just double click the word.
